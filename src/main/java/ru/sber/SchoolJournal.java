package ru.sber;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * School journal.
 */
public class SchoolJournal {

    private List<Person> persons;

    public SchoolJournal() {
        this.persons = new ArrayList<>();
    }

    public List<Person> getPersons() {
        return persons;
    }

    public void addPerson(Person person) {
        persons.add(person);
    }

    public Iterator<Person> courseIterator(int courseNumber) {
        return new PersonIteratorOverCourse(courseNumber);
    }

    private class PersonIteratorOverCourse implements Iterator<Person> {

        private  int pos = 0;
        private int courseNumber = 0;

        public PersonIteratorOverCourse(int courseNumber) {
            this.courseNumber = courseNumber;
        }

        private boolean canRemove = false;
        private int index = 0;

        @Override
        public boolean hasNext() {

            while(pos < persons.size()) {
                Person value = persons.get(pos);
                if (this.courseNumber == value.getCourse()) {

                    return true;
                }
                pos++;
            }
            return false;
        }

        @Override
        public Person next() {
            if (hasNext()) {
                Person value = persons.get(pos);
                index = pos;
                ++pos;
                canRemove = true;
                return value;
            }
            throw new NoSuchElementException("It after tail");
        }

        @Override
        public void remove() {

            if(!canRemove){
                throw new IllegalStateException("Can't remove");
            }
            Person value = persons.get(index);
            persons.remove(value);
            pos--;
            canRemove = false;

        }
    }
}
