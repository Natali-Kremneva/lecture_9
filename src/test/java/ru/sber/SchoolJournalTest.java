package ru.sber;

import org.junit.Assert;
import org.junit.Test;

import java.util.Iterator;
import java.util.NoSuchElementException;


public class SchoolJournalTest {

    @Test(expected = NoSuchElementException.class)
    public void nextThrowExceptin_Test() {
        SchoolJournal journal = new SchoolJournal();

        journal.addPerson(new Person("Ivan", "Ivanov", 1));
        journal.addPerson(new Person("Petr", "Petrov", 3));
        journal.addPerson(new Person("Georgy", "Georgov", 4));
        journal.addPerson(new Person("Maria", "Mikhailova", 1));
        journal.addPerson(new Person("Nikolai", "Sidorov", 2));
        journal.addPerson(new Person("Elena", "Ivanova", 3));
        journal.addPerson(new Person("Olga", "Koneva", 5));
        journal.addPerson(new Person("Ivan", "Petrov", 3));
        journal.addPerson(new Person("Petr", "Ivanov", 4));
        journal.addPerson(new Person("Sergei", "Semenov", 2));
        journal.addPerson(new Person("Semen", "Sergeev", 1));

        Iterator<Person> it = journal.courseIterator(4);

        while (it.hasNext()) {
            Person value = it.next();
        }
        it.next();
    }

    @Test
    public void next_Test() {
        SchoolJournal journal = new SchoolJournal();

        journal.addPerson(new Person("Ivan", "Ivanov", 1));
        journal.addPerson(new Person("Petr", "Petrov", 3));
        journal.addPerson(new Person("Georgy", "Georgov", 4));
        journal.addPerson(new Person("Maria", "Mikhailova", 1));
        journal.addPerson(new Person("Nikolai", "Sidorov", 2));
        journal.addPerson(new Person("Elena", "Ivanova", 3));
        journal.addPerson(new Person("Olga", "Koneva", 5));
        journal.addPerson(new Person("Ivan", "Petrov", 3));
        journal.addPerson(new Person("Petr", "Ivanov", 4));
        journal.addPerson(new Person("Sergei", "Semenov", 2));
        journal.addPerson(new Person("Semen", "Sergeev", 1));

        Iterator<Person> it = journal.courseIterator(4);

        while (it.hasNext()) {
            Person value = it.next();
            Assert.assertEquals(4, it.next().getCourse());
        }

    }

    @Test(expected = IllegalStateException.class)
    public void removeThrowExceptin_Test() {
        SchoolJournal journal = new SchoolJournal();

        journal.addPerson(new Person("Ivan", "Ivanov", 1));
        journal.addPerson(new Person("Petr", "Petrov", 3));
        journal.addPerson(new Person("Georgy", "Georgov", 4));
        journal.addPerson(new Person("Maria", "Mikhailova", 1));
        journal.addPerson(new Person("Nikolai", "Sidorov", 2));
        journal.addPerson(new Person("Elena", "Ivanova", 3));
        journal.addPerson(new Person("Olga", "Koneva", 5));
        journal.addPerson(new Person("Ivan", "Petrov", 3));
        journal.addPerson(new Person("Petr", "Ivanov", 4));
        journal.addPerson(new Person("Sergei", "Semenov", 2));
        journal.addPerson(new Person("Semen", "Sergeev", 1));

        Iterator<Person> it = journal.courseIterator(4);

        while (it.hasNext()) {
            Person value = it.next();
            it.remove();
        }
        it.remove();
    }

    @Test
    public void remove_Test() {
        SchoolJournal journal = new SchoolJournal();

        journal.addPerson(new Person("Ivan", "Ivanov", 1));
        journal.addPerson(new Person("Petr", "Petrov", 3));
        journal.addPerson(new Person("Georgy", "Georgov", 4));
        journal.addPerson(new Person("Maria", "Mikhailova", 1));
        journal.addPerson(new Person("Nikolai", "Sidorov", 2));
        journal.addPerson(new Person("Elena", "Ivanova", 3));
        journal.addPerson(new Person("Olga", "Koneva", 5));
        journal.addPerson(new Person("Ivan", "Petrov", 3));
        journal.addPerson(new Person("Petr", "Ivanov", 4));
        journal.addPerson(new Person("Sergei", "Semenov", 2));
        journal.addPerson(new Person("Semen", "Sergeev", 1));

        Iterator<Person> it = journal.courseIterator(4);

        Person person = it.next();

        it.remove();

        person = it.next();

        Assert.assertEquals("Petr",person.getFirstName());


    }
}